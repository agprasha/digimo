<?php

use yii\db\Migration;

class m191223_003521_004_create_table_user_meta extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user_meta}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'key' => $this->string(200),
            'value' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);

        $this->createIndex('user_id', '{{%user_meta}}', 'user_id');
        $this->addForeignKey('user_meta_ibfk_1', '{{%user_meta}}', 'user_id', '{{%user}}', 'id', 'RESTRICT', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%user_meta}}');
    }
}
