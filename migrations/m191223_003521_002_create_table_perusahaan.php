<?php

use yii\db\Migration;

class m191223_003521_002_create_table_perusahaan extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%perusahaan}}', [
            'id' => $this->primaryKey(),
            'nama_perusahaan' => $this->string(100),
            'email_perusahaan' => $this->string(100),
            'telpn_perusahaan' => $this->string(50),
            'npwp_perusahaan' => $this->string(100),
            'deskripsi_perusahaan' => $this->text(),
            'bidang_usaha' => $this->string(250),
            'alamat_perusahaan' => $this->text(),
            'provinsi' => $this->string(30),
            'kota' => $this->string(30),
            'kecamatan' => $this->string(30),
            'kelurahan' => $this->string(30),
            'kode_pos' => $this->string(10),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'deleted_at' => $this->integer(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%perusahaan}}');
    }
}
