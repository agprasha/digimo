<?php

use yii\db\Migration;

class m191223_003521_010_create_table_struktur extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%struktur}}', [
            'id' => $this->primaryKey(),
            'perusahaan_id' => $this->integer(),
            'jabatan_id' => $this->integer(),
            'user_id' => $this->integer(),
            'parent_id' => $this->integer(),
            'tanggal_input' => $this->date(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'deleted_at' => $this->dateTime(),
        ], $tableOptions);

        $this->createIndex('jabatan_id', '{{%struktur}}', 'jabatan_id');
        $this->createIndex('perusahaan_id', '{{%struktur}}', 'perusahaan_id');
        $this->createIndex('user_id', '{{%struktur}}', 'user_id');
        $this->addForeignKey('struktur_ibfk_1', '{{%struktur}}', 'perusahaan_id', '{{%perusahaan}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('struktur_ibfk_2', '{{%struktur}}', 'jabatan_id', '{{%jabatan}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('struktur_ibfk_3', '{{%struktur}}', 'user_id', '{{%user}}', 'id', 'RESTRICT', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%struktur}}');
    }
}
