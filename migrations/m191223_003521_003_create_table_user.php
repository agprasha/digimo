<?php

use yii\db\Migration;

class m191223_003521_003_create_table_user extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'nama' => $this->string(150),
            'email' => $this->string(50),
            'username' => $this->string(50),
            'no_hp' => $this->string(20),
            'password_hash' => $this->string(),
            'auth_key' => $this->string(191),
            'password_reset_token' => $this->string(200),
            'verification_token' => $this->string(200),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'deleted_at' => $this->dateTime(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
