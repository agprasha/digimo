<?php

use yii\db\Migration;

class m191223_003521_009_create_table_log extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%log}}', [
            'id' => $this->primaryKey(),
            'perusahaan_id' => $this->integer(),
            'user_id' => $this->integer(),
            'module' => $this->string(150),
            'fungsi' => $this->string(150),
            'keterangan' => $this->text(),
            'tanggal' => $this->date(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'deleted_at' => $this->dateTime(),
        ], $tableOptions);

        $this->createIndex('perusahaan_id', '{{%log}}', 'perusahaan_id');
        $this->createIndex('user_id', '{{%log}}', 'user_id');
        $this->addForeignKey('log_ibfk_1', '{{%log}}', 'user_id', '{{%user}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('log_ibfk_2', '{{%log}}', 'perusahaan_id', '{{%perusahaan}}', 'id', 'RESTRICT', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%log}}');
    }
}
