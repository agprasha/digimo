<?php

use yii\db\Migration;

class m191223_003521_007_create_table_jabatan extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%jabatan}}', [
            'id' => $this->primaryKey(),
            'perusahaan_id' => $this->integer(),
            'nama' => $this->string(60)->notNull()->defaultValue(''),
            'singkatan' => $this->string(15),
            'role_perusahaan' => $this->string(100),
            'deskripsi' => $this->text(),
            'level' => $this->smallInteger(5)->notNull(),
            'icon' => $this->string(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'deleted_at' => $this->dateTime(),
        ], $tableOptions);

        $this->createIndex('perusahaan_id', '{{%jabatan}}', 'perusahaan_id');
        $this->createIndex('tbl_product_NK4', '{{%jabatan}}', 'level');
        $this->addForeignKey('jabatan_ibfk_1', '{{%jabatan}}', 'perusahaan_id', '{{%perusahaan}}', 'id', 'RESTRICT', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%jabatan}}');
    }
}
