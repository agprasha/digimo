<?php

use yii\db\Migration;

class m191223_003521_008_create_table_kode extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%kode}}', [
            'id' => $this->primaryKey(),
            'perusahaan_id' => $this->integer(),
            'kode' => $this->string(100),
            'nama' => $this->string(100),
            'keterangan' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'deleted_at' => $this->dateTime(),
        ], $tableOptions);

        $this->createIndex('perusahaan_id', '{{%kode}}', 'perusahaan_id');
        $this->addForeignKey('kode_ibfk_1', '{{%kode}}', 'perusahaan_id', '{{%perusahaan}}', 'id', 'RESTRICT', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%kode}}');
    }
}
