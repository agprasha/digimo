<?php

use yii\db\Migration;

class m191223_003521_012_create_table_keuangan extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%keuangan}}', [
            'id' => $this->primaryKey(),
            'status' => $this->tinyInteger(2),
            'perusahaan_id' => $this->integer(),
            'user_id' => $this->integer(),
            'created_by' => $this->integer(),
            'kode_id' => $this->integer(),
            'nominal' => $this->integer(21),
            'tanggal' => $this->date(),
            'keterangan' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'deleted_at' => $this->dateTime(),
        ], $tableOptions);

        $this->createIndex('kode_id', '{{%keuangan}}', 'kode_id');
        $this->createIndex('user_id', '{{%keuangan}}', 'user_id');
        $this->createIndex('perusahaan_id', '{{%keuangan}}', 'perusahaan_id');
        $this->addForeignKey('keuangan_ibfk_2', '{{%keuangan}}', 'user_id', '{{%user}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('keuangan_ibfk_3', '{{%keuangan}}', 'kode_id', '{{%kode}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('keuangan_ibfk_4', '{{%keuangan}}', 'perusahaan_id', '{{%perusahaan}}', 'id', 'RESTRICT', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{%keuangan}}');
    }
}
