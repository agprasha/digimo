<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'maskMoneyOptions' => [
        'prefix' => 'Rp. ',
        'suffix' => '',
        'affixesStay' => true,
        'thousands' => '.',
        'decimal' => ',',
        'precision' => 0, 
        'allowZero' => false,
        'allowNegative' => false,
    ]
];
