<?php

namespace app\models;

use Yii;
use yii\helpers\Json;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "user_meta".
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $key
 * @property string|null $value
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property Kode[] $kodes
 * @property Transaksi[] $transaksis
 * @property User $user
 */
class UserMeta extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_meta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'created_at', 'updated_at'], 'integer'],
            [['value'], 'string'],
            [['key'], 'string', 'max' => 200],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'key' => Yii::t('app', 'Key'),
            'value' => Yii::t('app', 'Value'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKodes()
    {
        return $this->hasMany(Kode::className(), ['perusahaan_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaksis()
    {
        return $this->hasMany(Transaksi::className(), ['perusahaan_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                     ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                     ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
             ],
        ];
    }

    public static function findIdKaryawan($owner_id)
    {
        $karyawan = static::find()
        ->where(['user_id' => $owner_id])
        ->andWhere(['key' => '_data_karyawan'])
        ->select('value')
        ->one();
        
        return Json::decode($karyawan->value);
    }

    public function totalKaryawan()
    {
        return count(self::findIdKaryawan(self::findOwner(User::findUser()->id)->id));
    }

    public static function findOwner($karyawan_id)
    {
        $karyawan = static::find()
        ->where(['key' => '_data_karyawan'])
        ->andWhere(['LIKE', 'value', $karyawan_id])
        ->one();

        $owner = User::findOne($karyawan->user_id);

        return $owner;
    }

    public static function findDataPerusahaan($karyawan_id)
    {
        $owner = self::findOwner($karyawan_id);

        $dataPerusahaan = static::find()
        ->where(['key' => '_data_perusahaan'])
        ->andWhere(['user_id' => $owner->id])
        ->select('value')
        ->one();

        return (object)Json::decode($dataPerusahaan->value);
    }
}
