<?php

namespace app\models;

use Yii;
use app\models\Log;
use app\models\Struktur;
use yii\db\ActiveRecord;
use app\models\Perusahaan;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "jabatan".
 *
 * @property int $id
 * @property int|null $perusahaan_id
 * @property string $nama
 * @property string $role_perusahaan
 * @property string|null $deskripsi
 * @property int $level
 * @property string|null $icon
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $deleted_at
 *
 * @property Perusahaan $perusahaan
 */
class Jabatan extends \yii\db\ActiveRecord
{
    public $listLevel = [
        '1' => 1,
        '2' => 2,
        '3' => 3,
        '4' => 4,
        '5' => 5,
        '6' => 6,
        '7' => 7,
        '8' => 8,
        '9' => 9,
        '10' => 10
    ];
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jabatan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['perusahaan_id', 'level', 'created_at', 'updated_at', 'deleted_at'], 'integer'],
            [['deskripsi', 'singkatan', 'role_perusahaan'], 'string'],
            [['level', 'nama', 'singkatan', 'deskripsi'], 'required', 'message' => '{attribute} wajib diisi'],
            [['nama'], 'string', 'max' => 60],
            [['icon'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'perusahaan_id' => Yii::t('app', 'Perusahaan'),
            'nama' => Yii::t('app', 'Nama'),
            'role_perusahaan' => Yii::t('app', 'Jabatan Perusahaan'),
            'singkatan' => Yii::t('app', 'Singkatan'),
            'deskripsi' => Yii::t('app', 'Deskripsi'),
            'level' => Yii::t('app', 'Level'),
            'icon' => Yii::t('app', 'Icon'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    public function findLevel($level)
    {
        return $this->listLevel[$level];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerusahaan()
    {
        return $this->hasOne(Perusahaan::className(), ['id' => 'perusahaan_id']);
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    public static function softDelete($id)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $jabatan = static::findOne($id);
            $jabatan->deleted_at = date('Y-m-d H:i:s');
            $jabatan->save(false);

            $createdBy = Yii::$app->user->identity;

            $log = new Log();
            $log->user_id = $createdBy->id;
            $log->perusahaan_id = Struktur::findOne(['user_id' => $createdBy->id])->perusahaan_id;
            $log->module = 'Jabatan';
            $log->fungsi = 'Delete';
            $log->keterangan = $createdBy->nama. ', menghapus Jabatan '.$jabatan->nama;
            $log->tanggal = date('Y-m-d');
            $log->save(false);

            $transaction->commit();

            return true;
        } catch (Exception $th) {
            $transaction->rollBack();
            return false;
        }
    }

    public static function listJabatan()
    {
        $jabatan = static::find()
        ->where(['perusahaan_id' => Perusahaan::findDataPerusahaan()->id])
        ->andWhere(['deleted_at' => NULL])
        ->all();

        return ArrayHelper::map($jabatan, 'id', 'nama');
    }
}
