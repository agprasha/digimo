<?php

namespace app\models;

use Yii;
use app\models\Log;
use yii\db\Exception;
use yii\helpers\Json;
use app\models\Jabatan;
use app\models\AuthItem;
use app\models\Struktur;
use app\models\UserMeta;
use yii\db\ActiveRecord;
use app\models\Perusahaan;
use yii\helpers\ArrayHelper;
use app\models\AuthAssignment;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string|null $nama
 * @property string|null $email
 * @property string|null $username
 * @property string|null $password_hash
 * @property string|null $auth_key
 * @property string|null $password_reset_token
 * @property string|null $verification_token
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property string|null $deleted_at
 *
 * @property Transaksi[] $transaksis
 * @property UserMeta[] $userMetas
 */

class User extends ActiveRecord implements IdentityInterface
{
    public $nama_perusahaan;
    public $telpn_perusahaan;
    public $alamat_perusahaan;
    public $email_perusahaan;
    public $new_password;
    public $jabatan_id;

    const scenario_create = 'create';
    const scenario_update = 'update';
    const scenario_register = 'register';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    public function requiredMessage()
    {
        return '{attribute} '.Yii::t('app', 'Wajib Diisi');
    }

    public function uniqueMessage()
    {
        return '{attribute} '.Yii::t('app', 'Sudah terdaftar');
    }

    public function rulesRegisterRequired()
    {
        return [
            [
                'nama', 
                'email', 
                'username', 
                'password_hash', 
                'nama_perusahaan', 
                'telpn_perusahaan', 
                'alamat_perusahaan',
                'email_perusahaan'
            ],
            'required', 
            'message' => self::requiredMessage(),
            'on' => 'register', 
        ];
    }

    public function rulesCreateRequired()
    {
        return [
            [
                'nama', 
                'email', 
                'username', 
                'password_hash', 
                'jabatan_id',
            ],
            'required', 
            'message' => self::requiredMessage(),
            'on' => 'create', 
        ];
    }

    public function rulesUpdateRequired()
    {
        return [
            [
                'nama', 
                'email', 
                'username',
                'jabatan_id'
            ],
            'required', 
            'message' => self::requiredMessage(),
            'on' => 'update', 
        ];
    }

    public function rulesRegisterUnique()
    {
        return [
            [
                'email', 
                'username',
            ],
            'unique',
            'message' => self::uniqueMessage(),
            'on' => 'register', 
        ];
    }

    public function rulesCreateUnique()
    {
        return [
            [
                'email', 
                'username',
            ],
            'unique',
            'message' => self::uniqueMessage(),
            'on' => 'create', 
        ];
    }

    public function rulesUpdateUnique()
    {
        return [
            [
                'email', 
                'username',
            ],
            'unique',
            'message' => self::uniqueMessage(),
            'on' => 'create', 
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at'], 'integer'],
            [['deleted_at'], 'safe'],
            [['nama'], 'string', 'max' => 150],
            [['email', 'username'], 'string', 'max' => 50],
            [['password_hash', 'new_password'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 191],
            [['password_reset_token', 'verification_token'], 'string', 'max' => 200],
            self::rulesRegisterRequired(),
            self::rulesRegisterUnique(),

            self::rulesCreateRequired(),
            self::rulesCreateUnique(),

            self::rulesUpdateRequired(),
            self::rulesUpdateUnique(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nama' => Yii::t('app', 'Nama'),
            'email' => Yii::t('app', 'Email'),
            'username' => Yii::t('app', 'Username'),
            'password_hash' => Yii::t('app', 'Password'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'jabatan_id' =>  Yii::t('app', 'Jabatan'),
            'nama_perusahaan' => Yii::t('app', 'Nama Perusahaan'),
            'telpn_perusahaan' => Yii::t('app', 'No Telepon Perusahaan'),
            'alamat_perusahaan' => Yii::t('app', 'Alamat Perusahaan'),
            'email_perusahaan' => Yii::t('app', 'Email Perusahaan'),
            'new_password' => Yii::t('app', 'Password'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'verification_token' => Yii::t('app', 'Verification Token'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }
    
    public function scenarios() {
        $scenarios = parent::scenarios();
        $scenarios['update'] = ['nama', 'email', 'username', 'new_password', 'jabatan_id'];
        $scenarios['create'] = ['password_hash', 'nama', 'email', 'username', 'jabatan_id'];
        $scenarios['register'] = [
            'nama', 
            'email',
            'username',
            'password_hash',
            'email_perusahaan',
            'nama_perusahaan',
            'telpn_perusahaan',
            'alamat_perusahaan'
        ];
        
        return $scenarios;
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                 'class' => 'yii\behaviors\TimestampBehavior',
                 'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                 ],
             ],
        ];
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKeuangans()
    {
        return $this->hasMany(Keuangan::className(), ['user_id' => 'id']);
    }
    

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserMetas()
    {
        return $this->hasMany(UserMeta::className(), ['user_id' => 'id']);
    }

    public function getStruktur()
    {
        return $this->hasOne(Struktur::className(), ['user_id' => 'id'])->where(['perusahaan_id' => Perusahaan::findDataPerusahaan()->id]);
    }

    public function getRole()
    {
        return array_keys(Yii::$app->authManager->getAssignments(Yii::$app->user->identity->id))[0];
    }

    public static function findUser()
    {
        return Yii::$app->user->identity;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'deleted_at' => null]);
    }

    public static function listUser()
    {
        $user = static::find()
        ->where(['deleted_at' => null])
        ->all();

        return ArrayHelper::map($user, 'id','nama');
    }
    

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['auth_key' => $token, 'deleted_at' => null]);
        // throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'deleted_at' => null]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'deleted_at' => null,
        ]);
    }

    /**
     * Finds user by verification email token
     *
     * @param string $token verify email token
     * @return static|null
     */
    public static function findByVerificationToken($token) {
        return static::findOne([
            'verification_token' => $token,
            'deleted_at' => null
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        return Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        return Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public function generateEmailVerificationToken()
    {
        $this->verification_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public function register()
    {
        if (!$this->validate()) {
            return null;
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $user = new User();
            $user->nama = $this->nama;
            $user->email = $this->email;
            $user->username = $this->username;
            $user->password_hash = $this->setPassword($this->password_hash);
            $user->auth_key = $this->generateAuthKey();
            $user->save(false);

            $perusahaan = new Perusahaan();
            $perusahaan->nama_perusahaan = $this->nama_perusahaan;
            $perusahaan->telpn_perusahaan = $this->telpn_perusahaan;
            $perusahaan->alamat_perusahaan = $this->alamat_perusahaan;
            $perusahaan->email_perusahaan = $this->email_perusahaan;
            $perusahaan->save(false);

            $jabatan = new Jabatan();
            $jabatan->perusahaan_id = $perusahaan->id;
            $jabatan->nama = 'Chief Executive Officer';
            $jabatan->singkatan = 'ceo';
            $jabatan->deskripsi = 'Pimpinan sebuah perusahaan';
            $jabatan->role_perusahaan = 'ceo_'.$perusahaan->id;
            $jabatan->level = 1;
            $jabatan->icon = NULL;
            $jabatan->save(false);

            $autItem = new AuthItem();
            $autItem->name = $jabatan->role_perusahaan;
            $autItem->type = 1;
            $autItem->description = $model->deskripsi;
            $autItem->save(false);

            $authAssigment = new AuthAssignment();
            $authAssigment->item_name = $jabatan->role_perusahaan;
            $authAssigment->user_id = (string)$user->id;
            $authAssigment->save(false);

            $permission = [
                [
                    'parent' => $jabatan->role_perusahaan,
                    'child' => 'Mengelola Jabatan'
                ],
                [
                    'parent' => $jabatan->role_perusahaan,
                    'child' => 'Mengelola Keuangan'
                ],
                [
                    'parent' => $jabatan->role_perusahaan,
                    'child' => 'Mengelola Kode'
                ],
                [
                    'parent' => $jabatan->role_perusahaan,
                    'child' => 'Mengelola Pengguna'
                ],
                [
                    'parent' => $jabatan->role_perusahaan,
                    'child' => '/site/index'
                ],
            ];

            Yii::$app->db
            ->createCommand()
            ->batchInsert('auth_item_child', ['parent','child'],$permission)
            ->execute();

            $struktur = new Struktur();
            $struktur->perusahaan_id = $perusahaan->id;
            $struktur->jabatan_id = $jabatan->id;
            $struktur->user_id = $user->id;
            $struktur->parent_id = NULL;
            $struktur->tanggal_input = date('Y-m-d');
            $struktur->save(false);

            $log = new Log();
            $log->user_id = $user->id;
            $log->perusahaan_id = $perusahaan->id;
            $log->module = 'Auth';
            $log->fungsi = 'Registrasi';
            $log->keterangan = $user->email.' Baru saja mendaftar';
            $log->tanggal = date('Y-m-d');
            $log->save();

            $transaction->commit();
            return true;
        } catch (Exception $th) {
            $transaction->rollBack();
            return false;
        }
    }

    public function create()
    {
        if (!$this->validate()) {
            return null;
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $user = new User();
            $user->nama = $this->nama;
            $user->email = $this->email;
            $user->username = $this->username;
            $user->password_hash = $this->setPassword($this->password_hash);
            $user->auth_key = $this->generateAuthKey();
            $user->save(false);

            $jabatan = Jabatan::findOne($this->jabatan_id);

            $authAssigment = new AuthAssignment();
            $authAssigment->item_name = $jabatan->role_perusahaan;
            $authAssigment->user_id = (string)$user->id;
            $authAssigment->save(false);

            $struktur = new Struktur();
            $struktur->perusahaan_id = Perusahaan::findDataPerusahaan()->id;
            $struktur->jabatan_id = $this->jabatan_id;
            $struktur->user_id = $user->id;
            $struktur->parent_id = NULL;
            $struktur->tanggal_input = date('Y-m-d');
            $struktur->save(false);

            $createdBy = Yii::$app->user->identity;

            $log = new Log();
            $log->user_id = $createdBy->id;
            $log->perusahaan_id = Perusahaan::findDataPerusahaan()->id;
            $log->module = 'User';
            $log->fungsi = 'Create';
            $log->keterangan = $createdBy->nama. ', menambahkan pengguna baru bernama '.$user->nama;
            $log->tanggal = date('Y-m-d');
            $log->save(false);

            $transaction->commit();
            return true;
            
        } catch (Exception $th) {
            $transaction->rollBack();
            return false;
        }
    }

    public function updateUser($id)
    {
        if (!$this->validate()) {
            return null;
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $user = User::findOne($id);
            $user->nama = $this->nama;
            $user->email = $this->email;
            $user->username = $this->username;
            if (!empty($this->new_password)) {
                $user->password_hash = $this->setPassword($this->new_password);
            }
            $user->save(false);

            $jabatan = Jabatan::findOne($this->jabatan_id);
            // debug($jabatan->role_perusahaan);
            $authAssigment = AuthAssignment::findOne([
                'user_id' => (string)$user->id,
                'item_name' => Jabatan::findOne($user->struktur->jabatan_id)->role_perusahaan
            ]);

            // debug($authAssigment);
            $authAssigment->item_name = $jabatan->role_perusahaan;
            $authAssigment->user_id = (string)$user->id;
            $authAssigment->save(false);

            $struktur = Struktur::findOne([
                'perusahaan_id' => Perusahaan::findDataPerusahaan()->id,
                'user_id' => $user->id
            ]);
            // $struktur->perusahaan_id = Perusahaan::findDataPerusahaan()->id;
            $struktur->jabatan_id = $this->jabatan_id;
            // $struktur->user_id = $user->id;
            $struktur->parent_id = NULL;
            $struktur->tanggal_input = date('Y-m-d');
            $struktur->save(false);

            $createdBy = Yii::$app->user->identity;

            $log = new Log();
            $log->user_id = $createdBy->id;
            $log->perusahaan_id = Perusahaan::findDataPerusahaan()->id;
            $log->module = 'User';
            $log->fungsi = 'Update';
            $log->keterangan = $createdBy->nama. ', memperbarui pengguna bernama '.$user->nama;
            $log->tanggal = date('Y-m-d');
            $log->save(false);

            $transaction->commit();
            return true;
        } catch (Exception $th) {
            $transaction->rollBack();
            return false;
        }
    }

    public function softDelete($id)
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $user = User::findOne($id);
            $user->deleted_at = date('Y-m-d H:i:s');
            $user->save(false);

            $createdBy = Yii::$app->user->identity;

            $log = new Log();
            $log->user_id = $createdBy->id;
            $log->module = 'User';
            $log->fungsi = 'Delete';
            $log->keterangan = $createdBy->nama. ', menghapus data karyawan '.$user->nama;
            $log->tanggal = date('Y-m-d');
            $log->save(false);

            $transaction->commit();

            return true;
        } catch (Exception $th) {
            $transaction->rollBack();
            return false;
        }
    }
}
