<?php

namespace app\models;

use Yii;
use app\models\Log;
use app\models\User;
use app\models\UserMeta;
use yii\db\ActiveRecord;
use app\models\Perusahaan;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "kode".
 *
 * @property int $id
 * @property int|null $perusahaan_id
 * @property string|null $kode
 * @property string|null $nama
 * @property string|null $keterangan
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property string|null $deleted_at
 *
 * @property UserMeta $perusahaan
 * @property Transaksi[] $transaksis
 */
class Kode extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'kode';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['perusahaan_id', 'created_at', 'updated_at'], 'integer'],
            [['keterangan'], 'string'],
            [['deleted_at'], 'safe'],
            [['kode', 'nama'], 'string', 'max' => 100],
            [['kode', 'nama', 'keterangan'], 'required', 'message' => '{attribute} wajib diisi'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'perusahaan_id' => Yii::t('app', 'Perusahaan'),
            'kode' => Yii::t('app', 'Kode'),
            'nama' => Yii::t('app', 'Nama'),
            'keterangan' => Yii::t('app', 'Keterangan'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerusahaan()
    {
        return $this->hasOne(UserMeta::className(), ['id' => 'perusahaan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransaksis()
    {
        return $this->hasMany(Transaksi::className(), ['kode_id' => 'id']);
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    public static function softDelete($id)
    {
        $transaction = Yii::$app->db->beginTransaction();

        try {
            $kode = static::findOne($id);
            $kode->deleted_at = date('Y-m-d H:i:s');
            $kode->save(false);

            $createdBy = Yii::$app->user->identity;

            $log = new Log();
            $log->user_id = $createdBy->id;
            $log->perusahaan_id = Perusahaan::findDataPerusahaan()->id;
            $log->module = 'Kode';
            $log->fungsi = 'Delete';
            $log->keterangan = $createdBy->nama. ', menghapus kode transaksi '.$kode->kode.' '.$kode->nama;
            $log->tanggal = date('Y-m-d');
            $log->save(false);

            $transaction->commit();

            return true;
        } catch (Exception $th) {
            $transaction->rollBack();
            return false;
        }
    }

    public static function listKode()
    {
        $kode = static::find()
        ->where(['perusahaan_id' => Perusahaan::findDataPerusahaan()->id])
        ->andWhere(['deleted_at' => null])
        ->all();

        return ArrayHelper::map($kode, 'id','nama');
    }
}
