<?php

namespace app\models;

use Yii;
use app\models\Struktur;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "perusahaan".
 *
 * @property int $id
 * @property string|null $nama_perusahaan
 * @property string|null $email_perusahaan
 * @property string|null $telpn_perusahaan
 * @property string|null $npwp_perusahaan
 * @property string|null $deskripsi_perusahaan
 * @property string|null $bidang_usaha
 * @property string|null $alamat_perusahaan
 * @property string|null $provinsi
 * @property string|null $kota
 * @property string|null $kecamatan
 * @property string|null $kelurahan
 * @property string|null $kode_pos
 * @property int|null $created_at
 * @property int|null $deleted_at
 * @property int|null $updated_at
 *
 * @property Jabatan[] $jabatans
 * @property Keuangan[] $keuangans
 * @property Kode[] $kodes
 * @property Log[] $logs
 */
class Perusahaan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'perusahaan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['deskripsi_perusahaan', 'alamat_perusahaan'], 'string'],
            [['created_at', 'deleted_at', 'updated_at'], 'integer'],
            [['nama_perusahaan', 'email_perusahaan', 'npwp_perusahaan'], 'string', 'max' => 100],
            [['telpn_perusahaan'], 'string', 'max' => 50],
            [['bidang_usaha'], 'string', 'max' => 250],
            [['provinsi', 'kota', 'kecamatan', 'kelurahan'], 'string', 'max' => 30],
            [['kode_pos'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nama_perusahaan' => Yii::t('app', 'Nama Perusahaan'),
            'email_perusahaan' => Yii::t('app', 'Email Perusahaan'),
            'telpn_perusahaan' => Yii::t('app', 'Telpn Perusahaan'),
            'npwp_perusahaan' => Yii::t('app', 'Npwp Perusahaan'),
            'deskripsi_perusahaan' => Yii::t('app', 'Deskripsi Perusahaan'),
            'bidang_usaha' => Yii::t('app', 'Bidang Usaha'),
            'alamat_perusahaan' => Yii::t('app', 'Alamat Perusahaan'),
            'provinsi' => Yii::t('app', 'Provinsi'),
            'kota' => Yii::t('app', 'Kota'),
            'kecamatan' => Yii::t('app', 'Kecamatan'),
            'kelurahan' => Yii::t('app', 'Kelurahan'),
            'kode_pos' => Yii::t('app', 'Kode Pos'),
            'created_at' => Yii::t('app', 'Created At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJabatans()
    {
        return $this->hasMany(Jabatan::className(), ['perusahaan_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKeuangans()
    {
        return $this->hasMany(Keuangan::className(), ['perusahaan_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKodes()
    {
        return $this->hasMany(Kode::className(), ['perusahaan_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogs()
    {
        return $this->hasMany(Log::className(), ['perusahaan_id' => 'id']);
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    public static function findDataPerusahaan()
    {
        return static::findOne(Struktur::findPerusahaan()->perusahaan_id);
    }
}
