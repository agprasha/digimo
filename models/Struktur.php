<?php

namespace app\models;

use Yii;
use app\models\User;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "struktur".
 *
 * @property int $id
 * @property int|null $perusahaan_id
 * @property int|null $jabatan_id
 * @property int|null $user_id
 * @property int|null $parent_id
 * @property string|null $tanggal_input
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property string|null $deleted_at
 */
class Struktur extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'struktur';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['perusahaan_id', 'jabatan_id', 'user_id', 'parent_id', 'created_at', 'updated_at'], 'integer'],
            [['tanggal_input', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'perusahaan_id' => Yii::t('app', 'Perusahaan ID'),
            'jabatan_id' => Yii::t('app', 'Jabatan ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'tanggal_input' => Yii::t('app', 'Tanggal Input'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    public static function findPerusahaan()
    {
        return static::findOne(['user_id' => User::findUser()->id]);
    }

    public static function findKaryawanId()
    {
        return static::find()
        ->where(['perusahaan_id' => self::findPerusahaan()->perusahaan_id])
        ->select('user_id')
        ->column();
    }

    public static function totalKaryawan()
    {
        return count(self::findKaryawanId());
    }
}
