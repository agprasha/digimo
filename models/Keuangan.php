<?php

namespace app\models;

use Yii;
use app\models\User;
use app\models\UserMeta;
use app\models\Perusahaan;

/**
 * This is the model class for table "keuangan".
 *
 * @property int $id
 * @property int|null $status
 * @property int|null $perusahaan_id
 * @property int|null $user_id
 * @property int|null $kode_id
 * @property int|null $nominal
 * @property string|null $tanggal
 * @property string|null $keterangan
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property string|null $deleted_at
 *
 * @property User $perusahaan
 * @property User $user
 * @property Kode $kode
 */
class Keuangan extends \yii\db\ActiveRecord
{
    const status_masuk = 1;
    const status_keluar = 2;

    public $listStatus = [
        self::status_masuk => 'Pemasukkan',
        self::status_keluar => 'Pengeluaran',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'keuangan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'perusahaan_id', 'user_id', 'kode_id', 'nominal', 'created_at', 'updated_at', 'created_by'], 'integer'],
            [['tanggal', 'deleted_at'], 'safe'],
            [['keterangan'], 'string'],
            [['kode', 'status', 'nominal', 'user_id', 'keterangan', 'kode_id'], 'required', 'message' => '{attribute} wajib diisi'],
            // [['perusahaan_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['perusahaan_id' => 'id']],
            // [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            // [['kode_id'], 'exist', 'skipOnError' => true, 'targetClass' => Kode::className(), 'targetAttribute' => ['kode_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'status' => Yii::t('app', 'Status'),
            'perusahaan_id' => Yii::t('app', 'Perusahaan'),
            'user_id' => Yii::t('app', 'Pengguna'),
            'kode_id' => Yii::t('app', 'Kode'),
            'created_by' => Yii::t('app', 'Diinput Oleh'),
            'nominal' => Yii::t('app', 'Nominal'),
            'tanggal' => Yii::t('app', 'Tanggal'),
            'keterangan' => Yii::t('app', 'Keterangan'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    public function listStatus($status)
    {
        return $this->listStatus[$status];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPerusahaan()
    {
        return $this->hasOne(User::className(), ['id' => 'perusahaan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKode()
    {
        return $this->hasOne(Kode::className(), ['id' => 'kode_id']);
    }

    public function create()
    {
        if (!$this->validate()) {
            return null;
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $keuangan = new Keuangan();
            $keuangan->status = $this->status;
            $keuangan->kode_id = $this->kode_id;
            $keuangan->nominal = $this->nominal;
            $keuangan->tanggal = date('Y-m-d');
            $keuangan->user_id = $this->user_id;
            $keuangan->created_by = User::findUser()->id;
            $keuangan->perusahaan_id = Perusahaan::findDataPerusahaan()->id;
            $keuangan->keterangan = $this->keterangan;
            $keuangan->save(false); 

            $log = new Log();
            $log->user_id = User::findUser()->id;
            $log->perusahaan_id = Perusahaan::findDataPerusahaan()->id;
            $log->module = 'Keuangan';
            $log->fungsi = 'Create';
            $log->keterangan = User::findUser()->nama. ', menambah transaksi keuangan';
            $log->tanggal = date('Y-m-d');
            $log->save(false);

            $transaction->commit();
            return true;
        } catch (Exception $th) {
            $transaction->rollBack();
            return false;
        }
    }

    public static function sumPengeluaran($startDate, $endDate)
    {
        return static::find()
        ->where(['between', 'tanggal', $startDate, $endDate])
        ->andWhere(['perusahaan_id' => Perusahaan::findDataPerusahaan()->id])
        ->andWhere(['status' => self::status_keluar])
        ->sum('nominal');
    }

    public static function sumPemasukkan($startDate, $endDate)
    {
        return static::find()
        ->where(['between', 'tanggal', $startDate, $endDate])
        ->andWhere(['perusahaan_id' => Perusahaan::findDataPerusahaan()->id])
        ->andWhere(['status' => self::status_masuk])
        ->sum('nominal');
    }

    public function sumKeuangan()
    {
        $pemasukkan = static::find()
        ->andWhere(['perusahaan_id' => Perusahaan::findDataPerusahaan()->id])
        ->andWhere(['status' => self::status_masuk])
        ->sum('nominal');

        $pengeluaran = static::find()
        ->andWhere(['perusahaan_id' => Perusahaan::findDataPerusahaan()->id])
        ->andWhere(['status' => self::status_keluar])
        ->sum('nominal');

        return $pemasukkan - $pengeluaran;
    }

    public static function sumAsset()
    {
        return static::find()
        ->andWhere(['perusahaan_id' => Perusahaan::findDataPerusahaan()->id])
        ->andWhere(['kode_id' => 10])
        ->sum('nominal');
    }
}
