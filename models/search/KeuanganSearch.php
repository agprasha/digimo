<?php

namespace app\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Keuangan;

/**
 * KeuanganSearch represents the model behind the search form of `app\models\Keuangan`.
 */
class KeuanganSearch extends Keuangan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'perusahaan_id', 'kode_id', 'nominal', 'created_at', 'updated_at'], 'integer'],
            [['tanggal', 'keterangan', 'deleted_at', 'user_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Keuangan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith(['user']);

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'perusahaan_id' => $this->perusahaan_id,
            'user_id' => $this->user_id,
            'kode_id' => $this->kode_id,
            'nominal' => $this->nominal,
            'tanggal' => $this->tanggal,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
        ]);

        $query->orFilterWhere(['LIKE', 'user.nama', $this->user_id]);
        // ->andFilterWhere(['like', 'keterangan', $this->keterangan])
        

        return $dataProvider;
    }
}
