<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Keuangan */

$this->title = Yii::t('app', 'Memperbarui {modelClass}: ', [
    'modelClass' => 'Keuangan',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Keuangan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Memperbarui');
?>
<div class="keuangan-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
