<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Keuangan */

$this->title = Yii::t('app', 'Tambah Keuangan');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Keuangan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="keuangan-create">
    <?= $this->render('_form', [
        'model' => $model,
        'kode' => $kode,
        'listUser' => $listUser
    ]) ?>
</div>
