<html>
    <meta http-equiv="Content-Type" content="text/html; charset=Windows-1252">
    <body>
        <h3><?= Yii::t('app', 'Laporan Keuangan '). $perusahaan->nama_perusahaan?></h3>
        <h4><?= indonesian_date($startDate, 'j F Y')?> - <?= indonesian_date($endDate, 'j F Y')?></h4>
        <?php if(!empty($query)) { ?>
            <table border="1" cellpadding="5" style="font-size:10px;font-family:tahoma,arial;" cellspacing="0">
                <thead>
                    <tr>
                        <th><?= Yii::t('app', 'Tanggal')?></th>
                        <th><?= Yii::t('app', 'Kode')?></th>
                        <th><?= Yii::t('app', 'Keterangan')?></th>
                        <th><?= Yii::t('app', 'Diinput Oleh')?></th>
                        <th><?= Yii::t('app', 'Vol Ke')?></th>
                        <th><?= Yii::t('app', 'Pemasukan')?></th>
                        <th><?= Yii::t('app', 'Pengeluaran')?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($query as $key => $value): ?>
                        <tr>
                            <td><?= indonesian_date($value->tanggal, 'l, j F Y') ?></td>
                            <td><?= $value->kode->kode.' - '.$value->kode->nama?></td>
                            <td><?= $value->keterangan ?></td>
                            <td><?= $value->user->nama ?></td>
                            <td><?= $key+1 ?></td>
                            <td>
                                <?= ($value->status == 1) ? Yii::$app->formatter->asCurrency($value->nominal) : '' ?>
                            </td>
                            <td>
                                <?= ($value->status == 2) ? Yii::$app->formatter->asCurrency($value->nominal) : '' ?>
                            </td>
                        </tr>
                    <?php endforeach ?>
                    <tr>
                        <td colspan="5"><strong><?= Yii::t('app', 'Total')?></strong></td>
                        <td><?= Yii::$app->formatter->asCurrency($pemasukkan) ?></td>
                        <td><?= Yii::$app->formatter->asCurrency($pengeluaran) ?></td>
                    </tr>
                    <tr>
                        <td colspan="5"><strong><?= Yii::t('app', 'Selisih')?></strong></td>
                        <td colspan="2" style="text-align:center"><?= Yii::$app->formatter->asCurrency($selisih) ?></td>
                    </tr>
                </tbody>
            </table>
        <?php }else{?>
            <?= Yii::t('app', 'Data Tidak Ditemukan')?>
        <?php }?>
    </body>
</html>