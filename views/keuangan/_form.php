<?php

use yii\helpers\Html;
use kartik\money\MaskMoney;
use kartik\widgets\Select2;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Keuangan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="keuangan-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($model, 'status')->dropDownList($model->listStatus, ['prompt' => 'Pilih']) ?>

        <?= $form->field($model, 'kode_id')->widget(Select2::className(), [
            'data' => $kode,
            'options' => ['placeholder' => 'Pilih'],
            'pluginOptions' => [
               'allowClear' => true
            ],
        ]) ?>

        <?= $form->field($model, 'user_id')->widget(Select2::className(), [
            'data' => $listUser,
            'options' => ['placeholder' => 'Pilih'],
            'pluginOptions' => [
               'allowClear' => true
            ],
        ]) ?>

        <?= $form->field($model, 'nominal')->widget(MaskMoney::className()) ?>

        <?= $form->field($model, 'keterangan')->textarea(['rows' => 6]) ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton('<i class="fa fa-floppy-o" aria-hidden="true"></i> '.Yii::t('app', 'Simpan'), ['class' => 'btn btn-success btn-flat']) ?>
        <?= Html::a('<i class="fa fa-ban" aria-hidden="true"></i> '.Yii::t('app', 'Batal'),['keuangan/index'], ['class' => 'btn btn-danger btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
