<?php

use app\models\User;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;
use kartik\widgets\Select2;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use mdm\admin\components\Helper;
use kartik\daterange\DateRangePicker;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\KeuanganSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Keuangan');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="keuangan-index">
    <?php $form = ActiveForm::begin(['method' => 'GET', 'action' => ['keuangan/index']]); ?>
    <div class="box box-primary">
        <div class="box-header with-border">
            <h4><?= Yii::t('app', 'Cetak Laporan Keuangan')?></h4>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Tanggal</label>
                        <?= DateRangePicker::widget([
                            'name'=>'filter_tanggal',
                            'presetDropdown'=>true,
                            'hideInput'=>true,
                            
                            'pluginOptions' => [
                                'ranges' => [
                                    Yii::t('app', 'Hari ini') => ["moment().startOf('day')", "moment().endOf('day')"],
                                    Yii::t('app', 'Kemarin') => ["moment().startOf('day').subtract(1,'days')", "moment().endOf('day').subtract(1,'days')"],
                                    Yii::t('app', '{n} Hari Terakhir', ['n' => 7]) => ["moment().startOf('day').subtract(6, 'days')", "moment().endOf('day')"],
                                    Yii::t('app', '{n} Hari Terakhir', ['n' => 30]) => ["moment().startOf('day').subtract(29, 'days')", "moment().endOf('day')"],
                                    Yii::t('app', 'Bulan Ini') => ["moment().startOf('month')", "moment().endOf('month')"],
                                    Yii::t('app', '1 Bulan Terakhir') => ["moment().subtract(1, 'month').startOf('month')", "moment().subtract(1, 'month').endOf('month')"],
                                    Yii::t('app', '3 Bulan Terakhir') => ["moment().subtract(3, 'month').startOf('month')", "moment().subtract(1, 'month').endOf('month')"],
                                    Yii::t('app', '6 Bulan Terakhir') => ["moment().subtract(6, 'month').startOf('month')", "moment().subtract(1, 'month').endOf('month')"],
                                    Yii::t('app', '12 Bulan Terakhir') => ["moment().subtract(12, 'month').startOf('month')", "moment().subtract(1, 'month').endOf('month')"]
                                ],
                            ],
                        ]);?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <button style="margin-top:24px;" type="submit" class="btn btn-primary btn-flat btn-block"><i class="fa fa-download" aria-hidden="true"></i> <?= Yii::t('app', 'Cetak')?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
    
    
    <div class="box box-primary">
        <?php Pjax::begin(['enablePushState' => false]); ?>
        <div class="box-header with-border">
            <!-- <?= Html::a(Yii::t('app', 'Create Keuangan'), ['create'], ['class' => 'btn btn-success btn-flat']) ?> -->
        </div>
        <div class="box-body table-responsive no-padding">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'layout' => "{items}\n{summary}\n{pager}",
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    // 'id',
                    [
                        'attribute' => 'status',
                        'value' => function($model){
                            return $model->listStatus($model->status);
                        },
                        'filter' => $searchModel->listStatus
                    ],
                    // 'perusahaan_id',
                    [
                        'attribute' => 'user_id',
                        'value' => function($model){
                            return $model->user->nama;
                        }
                    ],
                    [
                        'attribute' => 'kode_id',
                        'value' => function($model){
                            return $model->kode->kode.' - '.$model->kode->nama;
                        },
                        'filter' => Select2::widget([
                            'model'=> $searchModel,
                            'attribute' => 'kode_id',
                            'data' => $listKode,
                            'options' => ['placeholder' => 'Cari'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]),
                    ],
                    [
                        'attribute' => 'nominal',
                        'value' => function($model){
                            return Yii::$app->formatter->asCurrency($model->nominal);
                        }
                    ],
                    'keterangan:ntext',
                    [
                        'attribute' => 'tanggal',
                        'value' =>function($model){
                            return indonesian_date($model->tanggal, 'j F Y');
                        },
                        'filter' => DatePicker::widget([
                            'attribute' => 'tanggal',
                            'model' => $searchModel,
                            'options' => ['placeholder' => 'Cari'],
                            'type' => DatePicker::TYPE_INPUT,
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'yyyy-mm-dd'
                            ]
                        ]),
                    ],

                    // 'created_at',
                    // 'updated_at',
                    // 'deleted_at',

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => Helper::filterActionColumn('{view} {update} {delete}'),
                        'buttons' => [
                            'view' => function($url, $model, $key){
                                return Html::a('<i class="fa fa-eye" aria-hidden="true"></i>', ['view', 'id' => encrypt($model->id)], ['class' => 'btn btn-info btn-flat']);
                            },
                            'update' => function($url, $model, $key){
                                return Html::a('<i class="fa fa-pencil-square-o" aria-hidden="true"></i> ', ['update', 'id' => encrypt($model->id)], ['class' => 'btn btn-warning flat']);
                            },
                            'delete' => function($url, $model, $key){
                                return Html::a('<i class="fa fa-trash" aria-hidden="true"></i>', ['delete', 'id' => encrypt($model->id)],[
                                    'class' => 'btn btn-danger btn-flat',
                                    'data' => [
                                        'confirm' => Yii::t('app', 'Anda yakin ingin menghapus data ini?'),
                                        'method' => 'post',
                                    ]
                                ]);
                            }
                        ],
                    ],
                ],
            ]); ?>
        </div>
        <?php Pjax::end(); ?>
    </div>
</div>
