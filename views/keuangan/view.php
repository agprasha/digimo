<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use mdm\admin\components\Helper;

/* @var $this yii\web\View */
/* @var $model app\models\Keuangan */

$this->title = Yii::t('app', 'Detail Keuangan');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Keuangan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="keuangan-view box box-primary">
    <div class="box-header">
        <?php 
            if (Helper::checkRoute('update')) {
                echo Html::a('<i class="fa fa-pencil-square-o" aria-hidden="true"></i> '.Yii::t('app', 'Edit'), ['update', 'id' => encrypt($model->id)], ['class' => 'btn btn-primary btn-flat']);
            }
        ?>
        <?php
            if (Helper::checkRoute('delete')) {
                echo Html::a('<i class="fa fa-trash" aria-hidden="true"></i> '.Yii::t('app', 'Hapus'), ['delete', 'id' => encrypt($model->id)], [
                    'class' => 'btn btn-danger btn-flat',
                    'data' => [
                        'confirm' => Yii::t('app', 'Anda yakin ingin menghapus data ini?'),
                        'method' => 'post',
                    ],
                ]);
            }
        ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'status',
                    'value' => function($model){
                        return $model->listStatus($model->status);
                    },
                ],
                // 'perusahaan_id',
                [
                    'attribute' => 'user_id',
                    'value' => function($model){
                        return $model->user->nama;
                    }
                ],
                [
                    'attribute' => 'kode_id',
                    'value' => function($model){
                        return $model->kode->kode.' - '.$model->kode->nama;
                    },
                ],
                [
                    'attribute' => 'nominal',
                    'value' => function($model){
                        return Yii::$app->formatter->asCurrency($model->nominal);
                    }
                ],
                'keterangan:ntext',
                [
                    'attribute' => 'tanggal',
                    'value' =>function($model){
                        return indonesian_date($model->tanggal, 'j F Y');
                    },
                ],
            ],
        ]) ?>
    </div>
</div>
