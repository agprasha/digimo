<?php

use yii\helpers\Url;

?>
<div class="row">
    <div class="col-lg-4 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-yellow">
        <div class="inner">
            <h3><?= $totalKaryawan?></h3>

            <p><?= Yii::t('app', 'Total Karyawan')?></p>
        </div>
        <div class="icon">
            <i class="fa fa-user-plus"></i>
        </div>
        <a href="<?= Url::to(['user/index'])?>" class="small-box-footer">
        <?= Yii::t('app', 'Selengkapnya')?> <i class="fa fa-arrow-circle-right"></i>
        </a>
        </div>
    </div>
    <div class="col-lg-4 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-green">
        <div class="inner">
            <h3><?= Yii::$app->formatter->asCurrency($sumKeuangan)?></h3>

            <p><?= Yii::t('app', 'Total Keuangan')?></p>
        </div>
        <div class="icon">
            <i class="fa fa-money"></i>
        </div>
        <a href="<?= Url::to(['keuangan/index'])?>" class="small-box-footer">
        <?= Yii::t('app', 'Selengkapnya')?> <i class="fa fa-arrow-circle-right"></i>
        </a>
        </div>
    </div>
    <div class="col-lg-4 col-xs-6">
        <!-- small box -->
        <div class="small-box bg-blue">
        <div class="inner">
            <h3><?= Yii::$app->formatter->asCurrency($sumAsset)?></h3>

            <p><?= Yii::t('app', 'Total Asset')?></p>
        </div>
        <div class="icon">
            <i class="fa fa-shopping-cart"></i>
        </div>
        <a href="<?= Url::to(['keuangan/index'])?>" class="small-box-footer">
        <?= Yii::t('app', 'Selengkapnya')?> <i class="fa fa-arrow-circle-right"></i>
        </a>
        </div>
    </div>
</div>