<?php

use app\models\User;
use yii\helpers\Url;



/* @var $this yii\web\View */

$this->title = $findPerusahaan->nama_perusahaan;
?>
<div class="site-index">

    <?php 
        echo $this->render('widget', [
            'totalKaryawan' => $totalKaryawan,
            'sumKeuangan' => $sumKeuangan,
            'sumAsset' => $sumAsset
        ]);
    ?>
</div>
