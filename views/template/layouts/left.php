<?php

use app\models\User;
use dmstr\widgets\Menu;
use mdm\admin\components\Helper;

?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= User::findUser()->nama?></p>

                <!-- <a href="#"><i class="fa fa-circle text-success"></i> Online</a> -->
            </div>
        </div>

        <!-- search form -->
        <!-- <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form> -->
        <!-- /.search form -->
        <?php
            $menuItems = [
                ['label' => Yii::t('app', 'Menu Navigation'), 'options' => ['class' => 'header']],
                ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],

                [
                    'label' => Yii::t('app', 'Beranda'),
                    'icon' => 'fas fa-home',
                    'url' => ['/site/index'],
                ],
                [
                    'label' => Yii::t('app', 'Jabatan'),
                    'icon' => 'fas fa-sitemap',
                    'url' => '#',
                    'items' => [
                        [
                            'label' => Yii::t('app', 'List'),
                            'icon' => 'fas fa-list',
                            'url' => ['/jabatan/index'],
                        ],
                        [
                            'label' => Yii::t('app', 'Tambah'),
                            'icon' => 'fas fa-plus',
                            'url' => ['/jabatan/create'],
                        ],
                    ],
                ],
                [
                    'label' => Yii::t('app', 'Pengguna'),
                    'icon' => 'fas fa-user',
                    'url' => '#',
                    'items' => [
                        [
                            'label' => Yii::t('app', 'List Pengguna'),
                            'icon' => 'fas fa-list',
                            'url' => ['/user/index'],
                        ],
                        [
                            'label' => Yii::t('app', 'Tambah Pengguna'),
                            'icon' => 'fas fa-user-plus',
                            'url' => ['/user/create'],
                        ],
                    ],
                ],
                [
                    'label' => Yii::t('app', 'Hak Akses'), 
                    'icon' => 'fas fa-user-secret', 
                    'url' => '#',
                    'items' => [
                        [
                            'label' => Yii::t('app', 'Route'),
                            'icon' => 'fas fa-circle-o',
                            'url' => ['/admin/route'],
                        ],
                        [
                            'label' => Yii::t('app', 'Permission'),
                            'icon' => 'fas fa-circle-o',
                            'url' => ['/admin/permission'],
                        ],
                        [
                            'label' => Yii::t('app', 'Role'),
                            'icon' => 'fas fa-circle-o',
                            'url' => ['/admin/role'],
                        ],
                        [
                            'label' => Yii::t('app', 'Assignment'),
                            'icon' => 'fas fa-circle-o',
                            'url' => ['/admin/assignment'],
                        ]
                    ],
                ],

                [
                    'label' => Yii::t('app', 'Kode Transaksi'),
                    'icon' => 'fas fa-book',
                    'url' => '#',
                    'items' => [
                        [
                            'label' => Yii::t('app', 'List Kode Keuangan'),
                            'icon' => 'fas fa-list',
                            'url' => ['/kode/index'],
                        ],
                        [
                            'label' => Yii::t('app', 'Tambah Kode'),
                            'icon' => 'fas fa-plus',
                            'url' => ['/kode/create'],
                        ],
                    ],
                ],

                [
                    'label' => Yii::t('app', 'Keuangan'),
                    'icon' => 'fas fa-money',
                    'url' => '#',
                    'items' => [
                        [
                            'label' => Yii::t('app', 'List'),
                            'icon' => 'fas fa-list',
                            'url' => ['/keuangan/index'],
                        ],
                        [
                            'label' => Yii::t('app', 'Tambah'),
                            'icon' => 'fas fa-plus',
                            'url' => ['/keuangan/create'],
                        ],
                    ],
                ],
                
            ];
        ?>
        
        <?php $menuItems = Helper::filter($menuItems); ?>
        <?= Menu::widget([
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => $menuItems
            ]) ?>

    </section>

</aside>
