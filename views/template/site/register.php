<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Daftar';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="login-box">
    <div class="login-logo">
        <a href="#"><b>Management Officer</b></a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg"><?= Yii::t('app', 'Silakan isi kolom-kolom berikut untuk mendaftar: ')?></p>

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($user, 'username')->textInput(['autofocus' => true]) ?>
        <?= $form->field($user, 'email')->textInput(['autofocus' => true]) ?>
        <?= $form->field($user, 'nama')->textInput(['autofocus' => true]) ?>

        <?= $form->field($user, 'nama_perusahaan')->textInput(['autofocus' => true]) ?>
        <?= $form->field($user, 'telpn_perusahaan')->textInput(['autofocus' => true]) ?>
        <?= $form->field($user, 'alamat_perusahaan')->textInput(['autofocus' => true]) ?>
        <?= $form->field($user, 'email_perusahaan')->textInput(['autofocus' => true]) ?>

        <?= $form->field($user, 'password_hash')->passwordInput() ?>

        
        <!-- <div class="col-xs-4"> -->
            <?= Html::submitButton(Yii::t('app', 'Daftar'), ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
        <!-- </div> -->


        <?php ActiveForm::end(); ?>
        <br>
        <!-- <div class="social-auth-links text-center">
            <p>- OR -</p>
            <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in
                using Facebook</a>
            <a href="#" class="btn btn-block btn-social btn-google-plus btn-flat"><i class="fa fa-google-plus"></i> Sign
                in using Google+</a>
        </div> -->
        <!-- /.social-auth-links -->

        <!-- <a href="#">I forgot my password</a><br> -->
        <a href="<?= Url::to(['site/login'])?>" class="text-center btn btn-info btn-block"><?= Yii::t('app', 'Masuk')?></a>

    </div>
    <!-- /.login-box-body -->
</div><!-- /.login-box -->
