<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Kode */

$this->title = Yii::t('app', 'Memperbarui {modelClass}: ', [
    'modelClass' => 'Kode',
]) . $model->kode;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Kode'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->kode, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Memperbarui');
?>
<div class="kode-update">

    <?= $this->render('_form', [
        'model' => $model,
        'perusahaan' => $perusahaan

    ]) ?>

</div>
