<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Kode */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kode-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <input type="hidden" name="Kode[perusahaan_id]" value="<?= $perusahaan->id ?>">

        <?= $form->field($model, 'kode')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'keterangan')->textarea(['rows' => 6]) ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton('<i class="fa fa-floppy-o" aria-hidden="true"></i> '.Yii::t('app', 'Simpan'), ['class' => 'btn btn-success btn-flat']) ?>
        <?= Html::a('<i class="fa fa-ban" aria-hidden="true"></i> '.Yii::t('app', 'Batal'),['kode/index'], ['class' => 'btn btn-danger btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
