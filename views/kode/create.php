<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Kode */

$this->title = Yii::t('app', 'Tambah Kode');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Kode'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kode-create">

    <?= $this->render('_form', [
    'model' => $model,
    'perusahaan' => $perusahaan
    ]) ?>

</div>
