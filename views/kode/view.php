<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use mdm\admin\components\Helper;

/* @var $this yii\web\View */
/* @var $model app\models\Kode */

$this->title = $model->kode;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Kode'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kode-view box box-primary">
    <div class="box-header">
        <?php 
            if (Helper::checkRoute('update')) {
                echo Html::a('<i class="fa fa-pencil-square-o" aria-hidden="true"></i> '.Yii::t('app', 'Edit'), ['update', 'id' => encrypt($model->id)], ['class' => 'btn btn-primary btn-flat']);
            }
        ?>
        <?php
            if (Helper::checkRoute('delete')) {
                echo Html::a('<i class="fa fa-trash" aria-hidden="true"></i> '.Yii::t('app', 'Hapus'), ['delete', 'id' => encrypt($model->id)], [
                    'class' => 'btn btn-danger btn-flat',
                    'data' => [
                        'confirm' => Yii::t('app', 'Anda yakin ingin menghapus data ini?'),
                        'method' => 'post',
                    ],
                ]);
            }
        ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                // 'id',
                // 'perusahaan_id',
                'kode',
                'nama',
                'keterangan:ntext',
                // 'created_at:datetime',
                // 'updated_at:datetime',
                // 'deleted_at',
            ],
        ]) ?>
    </div>
</div>
