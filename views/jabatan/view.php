<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;
use mdm\admin\components\Helper;
// debug($listPermission);
/* @var $this yii\web\View */
/* @var $model app\models\Jabatan */



$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Jabatan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="jabatan-view box box-primary">
    <div class="box-header">
        <?php 
            if (Helper::checkRoute('update')) {
                echo Html::a('<i class="fa fa-pencil-square-o" aria-hidden="true"></i> '.Yii::t('app', 'Edit'), ['update', 'id' => encrypt($model->id)], ['class' => 'btn btn-primary btn-flat']);
            }
        ?>
        <?php
            if (Helper::checkRoute('delete')) {
                echo Html::a('<i class="fa fa-trash" aria-hidden="true"></i> '.Yii::t('app', 'Hapus'), ['delete', 'id' => encrypt($model->id)], [
                    'class' => 'btn btn-danger btn-flat',
                    'data' => [
                        'confirm' => Yii::t('app', 'Anda yakin ingin menghapus data ini?'),
                        'method' => 'post',
                    ],
                ]);
            }
        ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                // 'id',
                // 'perusahaan_id',
                'nama',
                'singkatan',
                'deskripsi:ntext',
                'level',
                // 'icon',
                // 'created_at:datetime',
                // 'updated_at:datetime',
                // 'deleted_at',
            ],
        ]) ?>
    </div>
</div>

<div class="box box-primary">
    <div class="box-header">
            <h3><?= Yii::t('app', 'Hak Akses')?></h3>
    </div>
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body">

        <div class="form-group">
            <label><?= Yii::t('app', 'Hak Akses')?></label>
            <select name="hak_akses[]" class="form-control dual_select" multiple>
                <?php foreach ($listPermission as $key => $value) {?>
                    <option value="<?= $key ?>"
                        <?php foreach ($findRole as $keyRole => $role) {?>
                            <?php if($role->child == $key) {?>
                                selected="selected"
                            <?php }?>
                        <?php }?>
                    ><?= $key ?></option>
                <?php }?>
            </select>
        </div>
        
        
        <div class="box-footer">
            <?= Html::submitButton('<i class="fa fa-floppy-o" aria-hidden="true"></i> '.Yii::t('app', 'Simpan'), ['class' => 'btn btn-success btn-flat']) ?>
            <?= Html::a('<i class="fa fa-ban" aria-hidden="true"></i> '.Yii::t('app', 'Batal'),['jabatan/index'], ['class' => 'btn btn-danger btn-flat']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<?php
$script = <<< JS
    $('.dual_select').bootstrapDualListbox({
        selectorMinimalHeight: 400,
        nonSelectedListLabel: 'Non-selected',
        selectedListLabel: 'Selected',
        preserveSelectionOnMove: false,
        moveOnSelect: false,
    });
JS;
$this->registerJs($script);
?>

