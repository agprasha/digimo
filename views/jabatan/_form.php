<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Jabatan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jabatan-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'singkatan')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'deskripsi')->textarea(['rows' => 6]) ?>

        <?= $form->field($model, 'level')->dropDownList($model->listLevel, ['prompt' => 'Pilih']) ?>

    </div>
    <div class="box-footer">
        <?= Html::submitButton('<i class="fa fa-floppy-o" aria-hidden="true"></i> '.Yii::t('app', 'Simpan'), ['class' => 'btn btn-success btn-flat']) ?>
        <?= Html::a('<i class="fa fa-ban" aria-hidden="true"></i> '.Yii::t('app', 'Batal'),['jabatan/index'], ['class' => 'btn btn-danger btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
