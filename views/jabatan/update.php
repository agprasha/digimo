<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Jabatan */

$this->title = Yii::t('app', 'Memperbarui {modelClass}: ', [
    'modelClass' => 'Jabatan',
]) . $model->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Jabatan'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => encrypt($model->id)]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Memperbarui');
?>
<div class="jabatan-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
