<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\grid\GridView;
use mdm\admin\components\Helper;
/* @var $this yii\web\View */
/* @var $searchModel app\models\search\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'List Pengguna');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index box box-primary">
    <?php Pjax::begin(['enablePushState' => false]); ?>
    <div class="box-header with-border">
        <?php //Html::a(Yii::t('app', 'T Users'), ['create'], ['class' => 'btn btn-success btn-flat']) ?>
    </div>
    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                // 'id',
                'nama',
                'email:email',
                'username',
                // 'password_hash',
                // 'auth_key',
                // 'password_reset_token',
                // 'verification_token',
                // 'created_at',
                // 'updated_at',
                // 'deleted_at',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => Helper::filterActionColumn('{view} {update} {delete}'),
                    'buttons' => [
                        'view' => function($url, $model, $key){
                            return Html::a('<i class="fa fa-eye" aria-hidden="true"></i>', ['view', 'id' => encrypt($model->id)], ['class' => 'btn btn-info btn-flat']);
                        },
                        'update' => function($url, $model, $key){
                            return Html::a('<i class="fa fa-pencil-square-o" aria-hidden="true"></i> ', ['update', 'id' => encrypt($model->id)], ['class' => 'btn btn-warning flat']);
                        },
                        'delete' => function($url, $model, $key){
                            return Html::a('<i class="fa fa-trash" aria-hidden="true"></i>', ['delete', 'id' => encrypt($model->id)],[
                                'class' => 'btn btn-danger btn-flat',
                                'data' => [
                                    'confirm' => Yii::t('app', 'Anda yakin ingin menghapus data ini?'),
                                    'method' => 'post',
                                ]
                            ]);
                        }
                    ],
                ],
            ],
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
