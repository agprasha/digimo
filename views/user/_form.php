<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'jabatan_id')->dropDownList($jabatan, ['prompt' => 'Pilih', 'value' => ($model->isNewRecord) ? '' : $model->struktur->jabatan_id])?>

        <?php if ($model->isNewRecord) {?>
            <?= $form->field($model, 'password_hash')->passwordInput(['maxlength' => true]) ?>
        <?php }else{?>
            <?= $form->field($model, 'new_password')->passwordInput(['maxlength' => true]) ?>
        <?php } ?>
        

    </div>
    <div class="box-footer">
        <?= Html::submitButton('<i class="fa fa-floppy-o" aria-hidden="true"></i> '.Yii::t('app', 'Simpan'), ['class' => 'btn btn-success btn-flat']) ?>
        <?= Html::a('<i class="fa fa-ban" aria-hidden="true"></i> '.Yii::t('app', 'Batal'),['user/index'], ['class' => 'btn btn-danger btn-flat']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
