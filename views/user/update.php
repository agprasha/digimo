<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Users */

$this->title = Yii::t('app', 'Memperbarui {modelClass}: ', [
    'modelClass' => Yii::t('app', 'Pengguna'),
]) . $model->username;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pengguna'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->username, 'url' => ['view', 'id' => encrypt($model->id)]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Memperbarui');
?>

<div class="users-update">
    <?= $this->render('_form', [
        'model' => $model,
        'jabatan' => $jabatan,
    ]) ?>
</div>