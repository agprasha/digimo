<?php

namespace app\controllers;

use Yii;
use app\models\Log;
use app\models\Kode;
use app\models\User;
use yii\web\Controller;
use app\models\UserMeta;
use app\models\Perusahaan;
use yii\filters\VerbFilter;
use app\models\search\KodeSearch;
use yii\web\NotFoundHttpException;

/**
 * KodeController implements the CRUD actions for Kode model.
 */
class KodeController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Kode models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new KodeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['deleted_at' => null]);
        $dataProvider->query->andWhere(['perusahaan_id' => Perusahaan::findDataPerusahaan()->id]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Kode model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel(decrypt($id)),
        ]);
    }

    /**
     * Creates a new Kode model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Kode();
        $perusahaan = Perusahaan::findDataPerusahaan();

        if ($model->load(Yii::$app->request->post())) {
            $model->save();
            
            $log = new Log();
            $log->user_id = User::findUser()->id;
            $log->perusahaan_id = Perusahaan::findDataPerusahaan()->id;
            $log->module = 'Kode';
            $log->fungsi = 'Create';
            $log->keterangan = User::findUser()->nama. ', menambah kode transaksi '.$model->kode.' '.$model->nama;
            $log->tanggal = date('Y-m-d');
            $log->save(false);

            Yii::$app->session->setFlash('success', Yii::t('app', 'Berhasil'));
            return $this->redirect(['view', 'id' => encrypt($model->id)]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'perusahaan' => $perusahaan
            ]);
        }
    }

    /**
     * Updates an existing Kode model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel(decrypt($id));
        $perusahaan = Perusahaan::findDataPerusahaan();

        if ($model->load(Yii::$app->request->post())) {

            $log = new Log();
            $log->user_id = User::findUser()->id;
            $log->perusahaan_id = Perusahaan::findDataPerusahaan()->id;
            $log->module = 'Kode';
            $log->fungsi = 'Update';
            $log->keterangan = User::findUser()->nama. ', memperbarui kode transaksi '.$model->kode.' '.$model->nama;
            $log->tanggal = date('Y-m-d');
            $log->save(false);

            $model->save();
            Yii::$app->session->setFlash('success', Yii::t('app', 'Berhasil'));
            return $this->redirect(['view', 'id' => encrypt($model->id)]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'perusahaan' => $perusahaan
            ]);
        }
    }

    /**
     * Deletes an existing Kode model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        Kode::softDelete(decrypt($id));

        Yii::$app->session->setFlash('success', Yii::t('app', 'Berhasil'));
        return $this->redirect(['index']);
    }

    /**
     * Finds the Kode model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Kode the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Kode::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'Halaman yang diminta tidak ada.'));
        }
    }
}
