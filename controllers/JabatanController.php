<?php

namespace app\controllers;

use Yii;
use app\models\Log;
use app\models\User;
use yii\db\Exception;
use app\models\Jabatan;
use yii\web\Controller;
use app\models\AuthItem;
use app\models\Struktur;
use app\models\Perusahaan;
use yii\filters\VerbFilter;
use app\models\AuthItemChild;
use yii\web\NotFoundHttpException;
use app\models\search\JabatanSearch;

/**
 * JabatanController implements the CRUD actions for Jabatan model.
 */
class JabatanController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Jabatan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new JabatanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['perusahaan_id' => Struktur::findPerusahaan()->perusahaan_id]);
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Jabatan model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $listPermission = AuthItem::listPermission();
        $model = $this->findModel(decrypt($id));
        $findRole = AuthItemChild::find()
        ->where(['parent' => $model->role_perusahaan])
        ->all();
        // debug($findRole);


        if (Yii::$app->request->post()) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $permission = Yii::$app->request->post('hak_akses');
                foreach ($permission as $key => $value) {
                    $data[] = [
                        'parent' => $model->role_perusahaan,
                        'child' => $value
                    ];
                }
                AuthItemChild::deleteAll(['parent' => $model->role_perusahaan]);

                Yii::$app->db
                ->createCommand()
                ->batchInsert('auth_item_child', ['parent','child'],$data)
                ->execute();

                $transaction->commit();
                Yii::$app->session->setFlash('success', Yii::t('app', 'Berhasil'));
                return $this->redirect(Yii::$app->request->referrer);
            } catch (Exception $th) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('error', Yii::t('app', 'Terjadi Kesalahan, silahkan coba lagi'));
                return $this->redirect(Yii::$app->request->referrer);
            }
        }
        
        return $this->render('view', [
            'model' => $model,
            'listPermission' => $listPermission,
            'findRole' => $findRole
        ]);
    }

    /**
     * Creates a new Jabatan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Jabatan();

        if ($model->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $model->perusahaan_id = Struktur::findPerusahaan()->perusahaan_id;
                $model->role_perusahaan = $_POST['Jabatan']['singkatan'].'_'.Perusahaan::findDataPerusahaan()->id;
                $model->save();

                $autItem = new AuthItem();
                $autItem->name = $model->singkatan.'_'.Perusahaan::findDataPerusahaan()->id;
                $autItem->type = 1;
                $autItem->description = $model->deskripsi;
                $autItem->save(false);

                $log = new Log();
                $log->user_id = User::findUser()->id;
                $log->perusahaan_id = Struktur::findPerusahaan()->perusahaan_id;
                $log->module = 'Jabatan';
                $log->fungsi = 'Create';
                $log->keterangan = User::findUser()->nama. ', membuat jabatan '.$model->nama;
                $log->tanggal = date('Y-m-d');
                $log->save(false);

                $transaction->commit();

                Yii::$app->session->setFlash('success', Yii::t('app', 'Berhasil'));
                return $this->redirect(['view', 'id' => encrypt($model->id)]);
                
            } catch (Exception $th) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('error', Yii::t('app', 'Terjadi Kesalahan, silahkan coba lagi'));
                return $this->redirect(Yii::$app->request->referrer);
            }
            
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Jabatan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel(decrypt($id));

        if ($model->load(Yii::$app->request->post())) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $model->perusahaan_id = Struktur::findPerusahaan()->perusahaan_id;
                $model->role_perusahaan = $_POST['Jabatan']['singkatan'].'_'.Perusahaan::findDataPerusahaan()->id;
                $model->save();

                $autItem = AuthItem::findOne(['name' => $model->singkatan]);
                $autItem->name = $model->singkatan;
                $autItem->description = $model->deskripsi;
                $autItem->save(false);

                $log = new Log();
                $log->user_id = User::findUser()->id;
                $log->perusahaan_id = Struktur::findPerusahaan()->perusahaan_id;
                $log->module = 'Jabatan';
                $log->fungsi = 'Update';
                $log->keterangan = User::findUser()->nama. ', memperbarui jabatan '.$model->nama;
                $log->tanggal = date('Y-m-d');
                $log->save(false);

                $transaction->commit();

                Yii::$app->session->setFlash('success', Yii::t('app', 'Berhasil'));
                return $this->redirect(['view', 'id' => decrypt($model->id)]);
            } catch (Exception $th) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('error', Yii::t('app', 'Terjadi Kesalahan, silahkan coba lagi'));
                return $this->redirect(Yii::$app->request->referrer);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Jabatan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        Jabatan::softDelete(decrypt($id));
        Yii::$app->session->setFlash('success', Yii::t('app', 'Berhasil'));
        return $this->redirect(['index']);
    }

    /**
     * Finds the Jabatan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Jabatan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Jabatan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'Halaman yang diminta tidak ada.'));
        }
    }
}
