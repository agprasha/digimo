<?php

namespace app\controllers;

use Yii;
use app\models\Kode;
use app\models\User;
use yii\web\Controller;
use app\models\Keuangan;
use app\models\UserMeta;
use app\models\Perusahaan;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use app\models\search\KeuanganSearch;

/**
 * KeuanganController implements the CRUD actions for Keuangan model.
 */
class KeuanganController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Keuangan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $perusahaan = Perusahaan::findDataPerusahaan();
        $searchModel = new KeuanganSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['perusahaan_id' => $perusahaan->id]);
        $listKode = Kode::listKode();

        if (Yii::$app->request->get('filter_tanggal')) {
            $tanggal = Yii::$app->request->get('filter_tanggal');
            $explode = explode(" - ", $tanggal);
            $startDate = $explode[0];
            $endDate = $explode[1];
            
            $keuangan = Keuangan::find()
            ->where(['between', 'tanggal', $startDate, $endDate])
            ->andWhere(['perusahaan_id' => $perusahaan->id])
            ->all();

            // debug($keuangan);

            $pemasukkan = Keuangan::sumPemasukkan($startDate, $endDate);
            $pengeluaran = Keuangan::sumPengeluaran($startDate, $endDate);
            $selisih = $pemasukkan - $pengeluaran;
            
            ob_get_clean();
            $filename = 'Laporan Keuangan '.$perusahaan->nama_perusahaan.' '.indonesian_date($startDate, 'j F Y').' - '.indonesian_date($endDate, 'j F Y').'.xls';
            header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            header("Content-Disposition: attachment;Filename=".$filename);

            if (!empty($keuangan)) {
                return $this->renderPartial('laporan', [
                    'query' => $keuangan,
                    'startDate' => $startDate,
                    'endDate' => $endDate,
                    'perusahaan' => $perusahaan,
                    'pemasukkan' => $pemasukkan,
                    'pengeluaran' => $pengeluaran,
                    'selisih' => $selisih
                ]);
            }else{
                Yii::$app->session->setFlash('error', Yii::t('app', 'Laporan keuangan tidak ditemukan'));
                return $this->redirect(['index']);
            }
            
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'listKode' => $listKode
        ]);
    }

    // public function laporan($tanggal)
    // {
    //     $explode = explode(" - ", $tanggal);
    //     $startDate = $explode[0];
    //     $endDate = $explode[1];
        
    //     $keuangan = Keuangan::find()
    //     ->where(['between', 'tanggal', $startDate, $endDate])
    //     ->andWhere(['perusahaan_id' => UserMeta::findOwner(User::findUser()->id)->id])
    //     ->all();

    //     $perusahaan = UserMeta::findDataPerusahaan(User::findUser()->id);

    //     $pemasukkan = Keuangan::sumPemasukkan($startDate, $endDate);
    //     $pengeluaran = Keuangan::sumPengeluaran($startDate, $endDate);
    //     $selisih = $pemasukkan - $pengeluaran;
    //     // debug($pengeluaran);
    //     // ob_start();
    //     ob_get_clean();
    //     $filename = 'Laporan Keuangan '.$perusahaan->nama_perusahaan.' '.indonesian_date($startDate, 'j F Y').' - '.indonesian_date($endDate, 'j F Y').'.xls';
    //     header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    //     header("Content-Disposition: attachment;Filename=".$filename);
    //     // ob_end_flush();

    //     return $this->renderPartial('laporan', [
    //         'query' => $keuangan,
    //         'startDate' => $startDate,
    //         'endDate' => $endDate,
    //         'perusahaan' => $perusahaan,
    //         'pemasukkan' => $pemasukkan,
    //         'pengeluaran' => $pengeluaran,
    //         'selisih' => $selisih
    //     ]);
    // }

    /**
     * Displays a single Keuangan model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel(decrypt($id)),
        ]);
    }

    /**
     * Creates a new Keuangan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Keuangan();
        $kode = Kode::listKode();
        $listUser = User::listUser();

        if ($model->load(Yii::$app->request->post()) && $model->create()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Berhasil'));

            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'kode' => $kode,
                'listUser' => $listUser
            ]);
        }
    }

    /**
     * Updates an existing Keuangan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel(decrypt($id));

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Berhasil'));

            return $this->redirect(['view', 'id' => decrypt($model->id)]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Keuangan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel(decrypt($model->id))->delete();
        Yii::$app->session->setFlash('success', Yii::t('app', 'Berhasil'));

        return $this->redirect(['index']);
    }

    /**
     * Finds the Keuangan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Keuangan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Keuangan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'Halaman yang diminta tidak ada.'));
        }
    }
}
