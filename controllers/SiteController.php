<?php

namespace app\controllers;

use Yii;
use app\models\User;
use yii\helpers\Url;
use yii\web\Response;
use yii\web\Controller;
use app\models\Keuangan;
use app\models\Struktur;
use app\models\LoginForm;
use app\models\Perusahaan;
use app\models\ContactForm;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            return $this->redirect(['login']);
        }
        
        $sumKeuangan = Keuangan::sumKeuangan();
        $totalKaryawan = Struktur::totalKaryawan();
        $findPerusahaan = Perusahaan::findDataPerusahaan(User::findUser()->id);
        $sumAsset = Keuangan::sumAsset();

        // debug(Keuangan::sumAsset());
        
        return $this->render('index', [
            'sumKeuangan' => $sumKeuangan,
            'totalKaryawan' => $totalKaryawan,
            'findPerusahaan' => $findPerusahaan,
            'sumAsset' => $sumAsset
        ]);
    }
    

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionRegister()
    {
        $user = new User();
        $user->scenario = 'register';

        

        if ($user->load(Yii::$app->request->post()) && $user->register()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Berhasil Mendaftar, silahkan login'));
            return $this->redirect(['login']);
        }

        return $this->render('register', [
            'user' => $user
        ]);
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
