<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\Jabatan;
use yii\web\Controller;
use app\models\Struktur;
use app\models\UserMeta;
use yii\filters\VerbFilter;
use app\models\search\UserSearch;
use yii\web\NotFoundHttpException;

/**
 * UserController implements the CRUD actions for Users model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Users models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['deleted_at' => null]);
        $dataProvider->query->andWhere(['IN', 'id', Struktur::findKaryawanId()]);
        // debug(Struktur::findKaryawanId());

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Users model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel(decrypt($id));
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Users model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();
        $model->scenario = 'create';
        $jabatan = Jabatan::listJabatan();

        if ($model->load(Yii::$app->request->post()) && $model->create()) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Berhasil'));
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'jabatan' => $jabatan
            ]);
        }
    }

    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel(decrypt($id));
        $model->scenario = 'update';
        $jabatan = Jabatan::listJabatan();

        if ($model->load(Yii::$app->request->post()) && $model->updateUser(decrypt($id))) {
            Yii::$app->session->setFlash('success', Yii::t('app', 'Berhasil'));
            return $this->redirect(['view', 'id' => encrypt($model->id)]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'jabatan' => $jabatan
            ]);
        }
    }

    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        User::softDelete(decrypt($id));
        
        Yii::$app->session->setFlash('success', Yii::t('app', 'Berhasil'));

        return $this->redirect(['index']);
    }

    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'Halaman yang diminta tidak ada.'));
        }
    }
}
